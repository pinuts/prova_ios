//
//  AppDelegate.h
//  Bookmarks
//
//  Created by André Carvalho on 29/03/12.
//  Copyright (c) 2012 Pinuts Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end

//
//  Bookmark.h
//  Bookmarks
//
//  Created by André Carvalho on 29/03/12.
//  Copyright (c) 2012 Pinuts Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bookmark : NSObject

-(id)initWithTitle:(NSString *)theTitle URLfromString:(NSString *)stringURL icon:(UIImage *)theIcon;

@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSURL *url;
@property (nonatomic,strong) UIImage *icon;

@end

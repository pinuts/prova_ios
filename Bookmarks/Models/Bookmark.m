//
//  Bookmark.m
//  Bookmarks
//
//  Created by André Carvalho on 29/03/12.
//  Copyright (c) 2012 Pinuts Studios. All rights reserved.
//

#import "Bookmark.h"

@implementation Bookmark
@synthesize url, title, icon;

-(id)initWithTitle:(NSString *)theTitle URLfromString:(NSString *)stringURL icon:(UIImage *)theIcon {
    if (self = [super init]) {
        self.title = theTitle;
        self.url = [NSURL URLWithString:stringURL];
        self.icon = theIcon;
    }
    return self;
}

@end

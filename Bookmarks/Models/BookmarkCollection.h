//
//  BookmarkCollection.h
//  Bookmarks
//
//  Created by André Carvalho on 29/03/12.
//  Copyright (c) 2012 Pinuts Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Bookmark;

@interface BookmarkCollection : NSObject <NSFastEnumeration> {
    NSMutableArray *bookmarks;
}

+(BookmarkCollection *)bookmarksTestCollection;

-(void)addBookmark:(Bookmark *)bookmark;
-(Bookmark *)objectAtIndex:(NSUInteger)index;
-(NSUInteger)count;

@end

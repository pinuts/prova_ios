//
//  BookmarkCollection.m
//  Bookmarks
//
//  Created by André Carvalho on 29/03/12.
//  Copyright (c) 2012 Pinuts Studios. All rights reserved.
//

#import "BookmarkCollection.h"
#import "Bookmark.h"

@implementation BookmarkCollection

+(BookmarkCollection *)bookmarksTestCollection {
    NSArray *titles = [NSArray arrayWithObjects:@"Google", @"Apple", @"Offspot" , nil];
    NSArray *urls = [NSArray arrayWithObjects:@"http://www.google.com", @"http://www.apple.com", 
                     @"http://www.offspot.com", nil];
    NSArray *images = [NSArray arrayWithObjects:@"google.png", @"apple.png", @"offspot.png", nil];
    
    BookmarkCollection *collection = [[BookmarkCollection alloc] init];
    
    for (NSUInteger i = 0; i < [titles count]; i++) {
        Bookmark *bookmark = [[Bookmark alloc] initWithTitle:[titles objectAtIndex:i]
                                               URLfromString:[urls objectAtIndex:i]
                                                        icon:[UIImage imageNamed:[images objectAtIndex:i]]];
        [collection addBookmark:bookmark];
    }
    
    return collection;
}

-(id)init {
    if (self = [super init]) {
        bookmarks = [NSMutableArray arrayWithCapacity:3];
    }
    return self;
}

-(void)addBookmark:(Bookmark *)bookmark {
    [bookmarks addObject:bookmark];
}

-(Bookmark *)objectAtIndex:(NSUInteger)index {
    return [bookmarks objectAtIndex:index];
}

-(NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id [])buffer count:(NSUInteger)len {
    return [bookmarks countByEnumeratingWithState:state objects:buffer count:len];
}

-(NSUInteger)count {
    return [bookmarks count];
}


@end
